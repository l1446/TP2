# TP2:  _Un service entouré d'un écosystème riche_

## Partie 1 : Mise en place de la solution

## Sommaire

- [Partie 1 : Mise en place de la solution](#partie-1--mise-en-place-de-la-solution)
  - [Sommaire](#sommaire)
- [I. Setup base de données](#i-setup-base-de-données)
  - [1. Install MariaDB](#1-install-mariadb)
  - [2. Conf MariaDB](#2-conf-mariadb)
  - [3. Test](#3-test)
- [II. Setup Apache](#ii-setup-apache)
  - [1. Install Apache](#1-install-apache)
    - [A. Apache](#a-apache)
    - [B. PHP](#b-php)
  - [2. Conf Apache](#2-conf-apache)
- [III. NextCloud](#iii-nextcloud)
  - [4. Test](#4-test)

# I. Setup base de données

## 1. Install MariaDB

🌞 **Le service MariaDB**

- Commande : `systemctl start mariadb`
- Commande : `systemctl enable mariadb`
- Commande : 
```bash
[toto@db ~]$ systemctl status mariadb
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; disabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-07 14:13:54 CET; 6s ago
     Docs: man:mysqld(8)
           https://mariadb.com/kb/en/library/systemd/
  Process: 1574 ExecStartPost=/usr/libexec/mysql-check-upgrade (code=exited, status=0/SUCCESS)
  Process: 1439 ExecStartPre=/usr/libexec/mysql-prepare-db-dir mariadb.service (code=exited, status=0/SUCCESS)
  Process: 1415 ExecStartPre=/usr/libexec/mysql-check-socket (code=exited, status=0/SUCCESS)
 Main PID: 1542 (mysqld)
   Status: "Taking your SQL requests now..."
    Tasks: 30 (limit: 4956)
   Memory: 105.9M
   CGroup: /system.slice/mariadb.service
           └─1542 /usr/libexec/mysqld --basedir=/usr

Dec 07 14:13:54 db.tp2.cesi mysql-prepare-db-dir[1439]: See the MariaDB Knowledgebase at http://mariadb.com/kb or >
Dec 07 14:13:54 db.tp2.cesi mysql-prepare-db-dir[1439]: MySQL manual for more instructions.
Dec 07 14:13:54 db.tp2.cesi mysql-prepare-db-dir[1439]: Please report any problems at http://mariadb.org/jira
Dec 07 14:13:54 db.tp2.cesi mysql-prepare-db-dir[1439]: The latest information about MariaDB is available at http:>
Dec 07 14:13:54 db.tp2.cesi mysql-prepare-db-dir[1439]: You can find additional information about the MySQL part a>
Dec 07 14:13:54 db.tp2.cesi mysql-prepare-db-dir[1439]: http://dev.mysql.com
Dec 07 14:13:54 db.tp2.cesi mysql-prepare-db-dir[1439]: Consider joining MariaDB's strong and vibrant community:
```

- MariaDB écoute sur le port 3306 :
```bash
[toto@db ~]$ sudo ss -lutpn
Netid     State       Recv-Q      Send-Q           Local Address:Port           Peer Address:Port     Process
tcp       LISTEN      0           128                    0.0.0.0:1088                0.0.0.0:*         users:(("sshd",pid=780,fd=5))
tcp       LISTEN      0           5                      0.0.0.0:8888                0.0.0.0:*         users:(("python3",pid=720,fd=3))
tcp       LISTEN      0           128                       [::]:1088                   [::]:*         users:(("sshd",pid=780,fd=7))
tcp       LISTEN      0           80                           *:3306                      *:*         users:(("mysqld",pid=1542,fd=21))
```

- Processus de mariadb et les utilisateurs les ayant lancés :
```bash
[toto@db ~]$ ps aux | grep mysql
mysql       1542  0.0 11.0 1299864 91232 ?       Ssl  14:13   0:00 /usr/libexec/mysqld --basedir=/usr
toto       1735  0.0  0.1 221928  1128 pts/0    R+   14:37   0:00 grep --color=auto mysql
```

---

🌞 **Firewall**

Ouverture du port 3306 sur db.tp2.cesi :
```bash
[toto@db ~]$ sudo firewall-cmd --permanent --add-port=3306/tcp
[sudo] password for toto:
success
[toto@db ~]$ sudo firewall-cmd --reload
success
[toto@db ~]$ sudo firewall-cmd --list-all
public (active)
  ports: 80/tcp 8888/tcp 2222/tcp 3306/tcp
```

## 2. Conf MariaDB

🌞 **Configuration élémentaire de la base**

- Exécution de la commande : mysql_secure_installation
````bash
[toto@db ~]$ mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? [Y/n] Y
New password:abc
Re-enter new password:abc
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] Y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] Y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] Y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] Y
 ... Success!

Cleaning up...

All done!  If you have completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
````
   
#
🌞 **Préparation de la base en vue de l'utilisation par NextCloud**

#
````bash
# Connexion à la base de données
# L'option -p indique que vous allez saisir un mot de passe
# Vous l'avez défini dans le mysql_secure_installation
[toto@db ~]$ sudo mysql -u root -p
[sudo] password for toto:
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 17
Server version: 10.3.28-MariaDB MariaDB Server
Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.
Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>
`````

Création Users

- Se connecter : sudo mysql -u root -p (le mot de passe sera demandé après)
```bash
MariaDB [(none)]> CREATE USER 'nextcloud'@'10.1.1.11' IDENTIFIED BY 'nya';
MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.2.1.11';
MariaDB [(none)]> FLUSH PRIVILEGES;
```

## 3. Test

🌞 **Installez sur la machine `web.tp2.cesi` la commande `mysql`**

````bash
[toto@web ~]$ dnf provides mysql
Rocky Linux 8 - AppStream                                                                                                    2.2 MB/s | 8.3 MB     00:03
Rocky Linux 8 - BaseOS                                                                                                       242 kB/s | 3.5 MB     00:14
Rocky Linux 8 - Extras                                                                                                        26 kB/s |  10 kB     00:00
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7

[toto@web ~]$ sudo dnf install mysql
[sudo] password for toto:
Complete!

[toto@web ~]$ mysql -h 10.2.1.12 -P 3306 -u nextcloud -p
Enter password:

Welcome to the MySQL monitor.  Commands end with ; or \g.
mysql> SHOW TABLES;
ERROR 1046 (3D000): No database selected
````
# II. Setup Apache

> La section II et III sont clairement inspirés de [la doc officielle de Rocky pour installer NextCloud](https://docs.rockylinux.org/guides/cms/cloud_server_using_nextcloud/).

Comme annoncé dans l'intro, on va se servir d'Apache dans le rôle de serveur Web dans ce TP2. Histoire de varier les plaisirs è_é

![Linux is a tipi](./pics/linux_is_a_tipi.jpg)

## 1. Install Apache

### A. Apache

🌞 **Installer Apache sur la machine `web.tp2.cesi`**

````bash
[toto@web ~]$ sudo dnf install httpd
[sudo] password for toto:
Complete!
````


🌞 **Analyse du service Apache**

- Activer Apache et le laisser se lancer au démarrage :
````bash
[toto@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
   Active: inactive (dead)
     Docs: man:httpd.service(8)
[toto@web ~]$ sudo systemctl enable --now httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
[toto@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-07 16:17:00 CET; 2s ago
     Docs: man:httpd.service(8)
 Main PID: 5380 (httpd)
   Status: "Started, listening on: port 80"
    Tasks: 213 (limit: 11234)
   Memory: 25.4M
   CGroup: /system.slice/httpd.service
           ├─5380 /usr/sbin/httpd -DFOREGROUND
           ├─5381 /usr/sbin/httpd -DFOREGROUND
           ├─5382 /usr/sbin/httpd -DFOREGROUND
           ├─5383 /usr/sbin/httpd -DFOREGROUND
           └─5384 /usr/sbin/httpd -DFOREGROUND

Dec 07 16:17:00 web.tp2.cesi systemd[1]: Starting The Apache HTTP Server...
Dec 07 16:17:00 web.tp2.cesi systemd[1]: Started The Apache HTTP Server.
Dec 07 16:17:00 web.tp2.cesi httpd[5380]: Server configured, listening on: port 80
````
- Le port d'écoute d'Apache est le 80 :

---

🌞 **Un premier test**

```bash
[toto@db ~]$ sudo firewall-cmd --permanent --add-port=80/tcp
[sudo] password for toto:
success
[toto@db ~]$ sudo firewall-cmd --reload
success
[toto@db ~]$ sudo firewall-cmd --list-all
public (active)
  ports: 80/tcp
```

````cmd
C:\Users\PépiteDeChocolat>curl 10.2.1.11
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
````


### B. PHP

NextCloud a besoin d'une version bien spécifique de PHP.  
Suivez **scrupuleusement** les instructions qui suivent pour l'installer.

🌞 **Installer PHP**

#
````bash
# ajout des dépôts EPEL
$ sudo dnf install epel-release
Complete!
$ sudo dnf update
# ajout des dépôts REMI
$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
Complete!
$ dnf module enable php:remi-7.4
Complete!

# install de PHP et de toutes les libs PHP requises par NextCloud
$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
````

---

## 2. Conf Apache

➜ Le fichier de conf principal utilisé par Apache est `/etc/httpd/conf/httpd.conf`.  
Il y en a plein d'autres : ils sont inclus par le fichier de conf principal.

➜ Dans Apache, il existe la notion de *VirtualHost*. On définit des *VirtualHost* dans les fichiers de conf d'Apache.  
On crée un *VirtualHost* pour chaque application web qu'héberge Apache.

> "Application Web" c'est le terme de hipster pour désigner un site web. Disons qu'aujourd'hui les sites peuvent faire tellement de trucs qu'on appelle plutôt ça une "application" plutôt qu'un "site". Une application web donc.

➜ Dans le dossier `/etc/httpd/` se trouve un dossier `conf.d`.  
Des dossiers qui se terminent par `.d`, vous en rencontrerez plein (pas que pour Apache) **ce sont des dossiers de *drop-in*.**

Plutôt que d'écrire 40000 lignes dans un seul fichier de conf, on éclate la configuration dans plusieurs fichiers.  
C'est + lisible et + facilement maintenable.

**Les dossiers de *drop-in* servent à accueillir ces fichiers de conf additionels.**  
Le fichier de conf principal a une ligne qui inclut tous les autres fichiers de conf contenus dans le dossier de *drop-in*.

---

🌞 **Analyser la conf Apache**

- mettez en évidence, dans le fichier de conf principal d'Apache, la ligne qui inclut tout ce qu'il y a dans le dossier de *drop-in* nommé `conf.d/`

````bash
[toto@web httpd]$ vi conf/httpd.conf
# Supplemental configuration
# Load config files in the "/etc/httpd/conf.d" directory, if any.
IncludeOptional conf.d/*.conf
````

🌞 **Créer un VirtualHost qui accueillera NextCloud**

- Création du fichier

````bash
[toto@web ~]$ sudo vi NextCloud.conf
```
```apache
<VirtualHost *:80>
  # on précise ici le dossier qui contiendra le site : la racine Web
  DocumentRoot /var/www/nextcloud/html/  

  # ici le nom qui sera utilisé pour accéder à l'application
  ServerName  web.tp2.cesi  

  <Directory /var/www/nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```
```bash
[toto@web ~]$ sudo systemctl restart httpd
[toto@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
  Drop-In: /usr/lib/systemd/system/httpd.service.d
           └─php74-php-fpm.conf
   Active: active (running) since Tue 2021-12-07 18:32:42 CET; 1s ago
````
---

🌞 **Configurer la racine web**

- Création du dossier au bon endroit :
````bash
[toto@web ~]$ cd /var/www/
[toto@web www]$ sudo mkdir nextcloud
[toto@web www]$ cd nextcloud/
[toto@web nextcloud]$ sudo mkdir html
````

- Modification des droits pour l'user apache :
```bash
[toto@web nextcloud]$ cd ..
[toto@web www]$ chown apache nextcloud/ -R
````

---

🌞 **Configurer PHP**

Récupération fuseau horaire + configuration du fichier php.ini :

```php
[toto@web www]$ timedatectl
               Local time: Tue 2021-12-07 18:59:51 CET
           Universal time: Tue 2021-12-07 17:59:51 UTC
                 RTC time: Tue 2021-12-07 19:00:37
                Time zone: Europe/Paris (CET, +0100)
System clock synchronized: no
              NTP service: active
          RTC in local TZ: no
````


NEXCTCLOUD


# III. NextCloud

On dit "installer NextCloud" mais en fait c'est juste récupérer les fichiers PHP, HTML, JS, etc... qui constituent NextCloud, et les mettre dans le dossier de la racine web.

🌞 **Récupérer Nextcloud**

````bash
# Petit tip : la commande cd sans argument permet de retourner dans votre homedir
[toto@web ~]$ cd

[toto@web ~]$ curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  148M  100  148M    0     0  5789k      0  0:00:26  0:00:26 --:--:-- 5169k

[toto@web ~]$ ls
nextcloud-21.0.1.zip
`````
#
🌞 **Ranger la chambre**

- Décompression de l'archive dans la racine web et suppression de celle-ci :

````bash
[toto@web ~]$ sudo unzip nextcloud-21.0.1.zip -d /var/www/html/
[toto@web ~]$ rm nextcloud-21.0.1.zip
````
---

## 4. Test

🌞 **Tester l'accès à NextCloud et finaliser son install'**

- Chemin du fichier hosts : C:\windows\system32\drivers\etc\hosts
- Rajouter à la suite : 10.1.1.11 web.tp2.cesi 


````bash
# localhost name resolution is handled within DNS itself.
#	127.0.0.1       localhost
#	::1             localhost
10.2.1.11		web.tp2.cesi
````

---

🌞 **Tester l'accès à NextCloud et finaliser son install'**

- ouvrez votre navigateur Web sur votre PC
- rdv à l'URL `http://web.tp2.cesi` ========> EXCELLENT !! Ça marche !!!!
- vous devriez avoir la page d'accueil de NextCloud
- ici deux choses :
  - les deux champs en haut pour créer un user admin au sein de NextCloud
  - le bouton "Configure the database" en bas
    - sélectionnez "MySQL/MariaDB"
    - entrez les infos pour que NextCloud sache comment se connecter à votre serveur de base de données
    - c'est les infos avec lesquelles vous avez validé à la main le bon fonctionnement de MariaDB (c'était avec la commande `mysql`)

---

**🔥🔥🔥 Baboom ! Un beau NextCloud.**

---
---
---
---
# Partie 2 : Sécurisation

On pourrait aborder beaucoup, beaucoup, beaucouuuuup de choses à ce sujet. De notre côté on va aller sur les aspects élémentaires.

> **QUEWA ?!** Vous voulez un truc complet sur comment sécuriser notre machin ? OK :D Le security guide de la doc officielle RedHat [**ICI**](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/security_guide/index) et le TRES bon "benchmark" de CIS Security [**ICI**](https://www.cisecurity.org/benchmark/centos_linux/). Gardez ça de côté pour quand vous aurez du temps pour lire ; en plus d'être des guides de sécurité, ce sont d'incroyables sources de connaissances.

## Sommaire

- [Partie 2 : Sécurisation](#partie-2--sécurisation)
  - [Sommaire](#sommaire)
- [I. Serveur SSH](#i-serveur-ssh)
  - [1. Conf SSH](#1-conf-ssh)
  - [2. Bonus : Fail2Ban](#2-bonus--fail2ban)
- [II. Serveur Web](#ii-serveur-web)
  - [1. Reverse Proxy](#1-reverse-proxy)
  - [2. HTTPS](#2-https)

# I. Serveur SSH

Dans cette partie on va renforcer un peu la sécurité autour du serveur SSH. Cela va se faire essentiellement au travers du fichier de conf `/etc/ssh/sshd_config`.

> L'ANSSI avait édité un guide de sécurité vraiment pas mal pour les serveurs SSH. Je parle au passé parce qu'il est un peu outdated (2015), [je vous le link quand même](https://www.ssi.gouv.fr/uploads/2014/01/NT_OpenSSH.pdf), en vrai la plupart des points restent d'actualité. Simplement, pour ce qui est des algos de chiffrement utilisés, ou la longueurs des clés conseillées, c'est plus vraiment à jour :(

## 1. Conf SSH

Si c'est pas fait vous **devez maintenant** configurer un accès par clés pour SSH.

🌞 **Modifier la conf du serveur SSH**

- Modification faite dans /etc/ssh/sshd_config :

````ssh
[toto@web conf.d]$ sudo vi /etc/ssh/sshd_config
# Aprés échange des clefs SSH inter-serveurs faire sur les deux machines
[toto@web ~]$ sudo vi /etc/ssh/sshd_config
PermitEmptyPasswords no
PasswordAuthentication no
PermitRootLogin no
````

- Rajouter tout en bas du fichier de conf pour forcer l'utilisation d'algo de chiffrement forts :

````ssh
# algorithms
KexAlgorithms curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256
MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,umac-128@openssh.com
Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr
hostkeyalgorithms ecdsa-sha2-nistp256-cert-v01@openssh.com,ecdsa-sha2-nistp384-cert-v01@openssh.com,ecdsa-sha2-nistp521-cert-v01@openssh.com,ssh-ed25519-cert-v01@openssh.com,rsa-sha2-512-cert-v01@openssh.com,rsa-sha2-256-cert-v01@openssh.com,ssh-rsa-cert-v01@openssh.com,ecdsa-sha2-nistp384,ecdsa-sha2-nistp521,ssh-ed25519,rsa-sha2-512,rsa-sha2-256
````
---

## 2. Bonus : Fail2Ban

Fail2Ban est un outil très simple :

- il lit les fichiers de logs en temps réel
- il repère des lignes en particulier, en fonction de patterns qu'on a défini
- si des lignes correspondant au même pattern sont répétées trop de fois, il effectue une action

Le cas typique :

- serveur SSH
- get attacked :(
- fail2ban détecte l'attaque rapidement, et ban l'IP source

Ca permet de se prémunir là encore d'attaques de masse : il faut que ça spam pour que fail2ban agisse. Des attaques qui essaient de bruteforce l'accès typiquement.

> C'est pas du tout que dans les séries Netflix hein. Vous mettez une machine avec une IP publique sur internet et c'est très rapide, vous vous faites attaquer toute la journée, tout le temps, plusieurs fois par minutes c'est pas choquant. Plus votre IP reste longtemps avec tel ou tel port ouvert, plus elle est "connue", et les attaques n'iront pas en diminuant.

🌞 **Installez et configurez fail2ban**

- Installation du fail2ban :
```bash
[toto@web ~]$ sudo dnf install fail2ban -y
Last metadata expiration check: 6:13:42 ago on Tue 07 Dec 2021 04:05:44 PM CET.
Dependencies resolved.
========================================================================================================================
 Package                            Architecture           Version                      Repository                 Size
========================================================================================================================
Installing:
 fail2ban                           noarch                 0.11.2-1.el8                 epel                       19 k
Installing dependencies:
[...]
Installed:
  esmtp-1.2-15.el8.x86_64                 fail2ban-0.11.2-1.el8.noarch          fail2ban-firewalld-0.11.2-1.el8.noarch
  fail2ban-sendmail-0.11.2-1.el8.noarch   fail2ban-server-0.11.2-1.el8.noarch   libesmtp-1.0.6-18.el8.x86_64
  liblockfile-1.14-1.el8.x86_64           python3-systemd-234-8.el8.x86_64

Complete!
[toto@web ~]$ sudo systemctl start fail2ban
[toto@web ~]$ sudo systemctl enable fail2ban
Created symlink /etc/systemd/system/multi-user.target.wants/fail2ban.service → /usr/lib/systemd/system/fail2ban.service.
[toto@web ~]$ sudo systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
   Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-07 22:19:50 CET; 14s ago
     Docs: man:fail2ban(1)
 Main PID: 3194 (fail2ban-server)
    Tasks: 3 (limit: 4956)
   Memory: 12.5M
   CGroup: /system.slice/fail2ban.service
           └─3194 /usr/bin/python3.6 -s /usr/bin/fail2ban-server -xf start

Dec 07 22:19:50 web.tp2.cesi systemd[1]: Starting Fail2Ban Service...
Dec 07 22:19:50 web.tp2.cesi systemd[1]: Started Fail2Ban Service.
Dec 07 22:19:50 web.tp2.cesi fail2ban-server[3194]: Server ready
```

- Test : https://doc.ubuntu-fr.org/fail2ban

# II. Serveur Web

Bah ui certains l'ont fait en bonus hier, mais comme ça, on met tout le monde à niveau. Libre à vous de sauter ça ou de le refaire (un peu plus en conscience avec le cours sur TLS ?) ou simplement de le faire si c'était pas fait avant !

## 1. Reverse Proxy

**Un reverse proxy c'est un serveur qui sert d'intermédiaire entre un client et un serveur, il est mis en place par l'administrateur du serveur.**

**Il a pour simple rôle de réceptionner les requêtes du client, et de les faire passer au serveur.** Nice, easy, and simple.

Maiiiis il peut évidemment permettre + de choses que ça :

- chiffrement (sécu, perfs)
- caching (perfs)
- répartition de charge (sécu)

**De façon générale, c'est lui qui est le "front" des applications Web**, faut que ce soit une machine robuste et secure. Il permet de centraliser les accès des clients aux applications Web.  
En effet, il n'est pas rare dans l'industrie de voir un nombre restreint de reverse proxies (mais bien bien solides) qui permettent d'accéder à une multitudes d'applications Web.

**N'hésitez pas, comme d'hab, à m'appeler si vous voulez une explication claire à l'oral de tout ça.**

---

🖥️ **Créez une nouvelle machine : `proxy.tp2.cesi`.** 🖥️

- Sur la nouvelle machine, voici les paramètres :
```
Adresse IP ens108 :  (LAN)
Adresse IP ens224 : 10.1.1.15 (Frontal)
Hostname : proxy.tp2.cesi
```

🌞 **Installer NGINX**

- NGINX déjà installé (mais non configuré), la VM est un clone du TP1 où nous avions déjà installé NGINX dessus.
- Démarrer le service :

```bash
[toto@proxy ~]$ sudo systemctl start nginx
[toto@proxy ~]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
[toto@proxy ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-12-08 09:49:54 CET; 9s ago
 Main PID: 1653 (nginx)
    Tasks: 2 (limit: 4956)
   Memory: 12.4M
   CGroup: /system.slice/nginx.service
           ├─1653 nginx: master process /usr/sbin/nginx
           └─1654 nginx: worker process

Dec 08 09:49:54 proxy.tp2.cesi systemd[1]: Starting The nginx HTTP and reverse proxy server...
Dec 08 09:49:54 proxy.tp2.cesi nginx[1649]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Dec 08 09:49:54 proxy.tp2.cesi nginx[1649]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Dec 08 09:49:54 proxy.tp2.cesi systemd[1]: nginx.service: Failed to parse PID from file /run/nginx.pid: Invalid argument
Dec 08 09:49:54 proxy.tp2.cesi systemd[1]: Started The nginx HTTP and reverse proxy server.
```


**Quelques infos :**

- les fichiers de log sont dans le dossier `/var/log/nginx/`
- la conf principale est dans `/etc/nginx/nginx.conf`
  - une des dernières lignes inclut des fichiers contenus dans `/etc/nginx/conf.d/`
  - n'hésitez pas à le lire le fichier, et à l'épurer des commentaires inutiles pour y voir plus clair
- n'oubliez pas la commande `sudo journalctl -xe -u <SERVICE>` pour obtenir les logs d'un service donné

🌞 **Configurer NGINX comme reverse proxy**

- **créez un nouveau fichier de conf dans `/etc/nginx/conf.d/` pour mettre votre conf de reverse proxy**
- je vous laisse chercher un peu par vous-mêmes pour ça
- je vous lâche quand même quelques liens :
  - [un article sympa](https://linuxize.com/post/nginx-reverse-proxy/#common-nginx-reverse-proxy-options) qui explique de façon claire la conf mise en place (particulièrement la section sur [les options récurrentes "common options"](https://linuxize.com/post/nginx-reverse-proxy/#common-nginx-reverse-proxy-options))
  - [ce très bon générateur de conf NGINX](https://www.digitalocean.com/community/tools/nginx#?)
    - c'est bien de l'avoir sous le coude, mais faut comprendre ce que ça fait, c'est mieux :)
  - [et pour le plaisir, j'ai pas de mots](https://docs.microsoft.com/en-us/troubleshoot/aspnetcore/2-2-install-nginx-configure-it-reverse-proxy) 
    - quand je l'ai vu, je suis vraiment parti d'une bonne intention, état d'esprit partial, mais non. Les screens dans une doc pour du terminal c'est nan
    - merci Microsoft, toujours le mot pour rire :') (le contenu est à peu près ok, dommage)

- J'ai utilisé le fichier de configuration par défaut, et j'ai changé les lignes suivantes :

````nginx
server {
    listen 80;
    server_name web.tp2.cesi;

    location / {
       proxy_pass http://web.tp2.cesi;
    proxy_http_version  1.1;
    proxy_cache_bypass  $http_upgrade;

    proxy_set_header Upgrade           $http_upgrade;
    proxy_set_header Connection        "upgrade";
    proxy_set_header Host              $host;
    proxy_set_header X-Real-IP         $remote_addr;
    proxy_set_header X-Forwarded-For   $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header X-Forwarded-Host  $host;
    proxy_set_header X-Forwarded-Port  $server_port;
    }
}
`````
#

🌞 **Une fois en place, test !**

- Modification du fichier hosts de mon poste physique en remplaçant :
````bash
[toto@proxy nginxconfig.io]$ sudo vi /etc/hosts
10.2.1.15   web.tp2.cesi
````
## 2. HTTPS

🌞 **Générer une clé et un certificat avec la commande suivante :**
#

````bash
[toto@proxy ~]$ openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -keyout server.key -out server.crt
Generating a RSA private key
....................................................................................................................++++
............................................++++
writing new private key to 'server.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:FR
State or Province Name (full name) []:Nouvelle-Aquitaine
Locality Name (eg, city) [Default City]:Bordeaux
Organization Name (eg, company) [Default Company Ltd]:CESI
Organizational Unit Name (eg, section) []:RISR16-1
Common Name (eg, your name or your server's hostname) []:web.tp2.cesi
Email Address []:PepiteDeChocolat@gnia.fr
````

---

**Par convention dans les systèmes RedHat :**

- on met les clés privées dans `/etc/pki/tls/private/`
  - permissions très restrictives
- les certificats (clés publiques) dans `/etc/pki/tls/certs/`
  - permissions moins restrictives : généralement y'a le droit de lecture pour tout le monde

Aussi, on nomme souvent le certificat et la clé en fonction du nom de domaine pour lesquels ils sont valides, en l'occurence `web.tp2.cesi.key` pour la clé par exemple.

🌞 **Allez, faut ranger la chambre**

- Renommage des clefs et déplacement de celles-ci suivant la convention :
#
````bash
[toto@proxy ~]$ sudo mv server.key /etc/pki/tls/private/web.tp2.cesi.key
[toto@proxy ~]$ sudo mv server.crt /etc/pki/tls/certs/web.tp2.cesi.crt
[toto@proxy ~]$ ls /etc/pki/tls/private
web.tp2.cesi.key
[toto@proxy ~]$ ls /etc/pki/tls/certs
ca-bundle.crt  ca-bundle.trust.crt  web.tp2.cesi.crt
````

---

🌞 **Affiner la conf de NGINX**

Direction le fichier de conf de votre reverse proxy.

- Il faudra ajouter/modifier les lignes suivantes :
#
````nginx
 server {
        listen       443;										
        listen       [::]:443;									
        server_name  web.tp2.cesi;

        ssl on;											

        ssl_certificate /etc/pki/tls/certs/web.tp2.cesi.crt;	
        ssl_certificate_key /etc/pki/tls/private/web.tp2.cesi.key;	
		[...]
    }
````
#
- Activons le port 443 pour pouvoir se connecter en HTTPS :

````bash
[toto@proxy ~]$ sudo firewall-cmd --add-port=443/tcp --permanent
success
[toto@proxy ~]$ sudo firewall-cmd --reload
success
````
#
---

🌞 **Test !**

- Connexion web : https://web.tp2.cesi => OK (mais certificat auto signé donc pas cool)

---
---
---
---

# Partie 3 : Maintien en condition opérationnelle

Ici aussi, on aurait pu aborder plein de choses, on va voir ici deux aspects :

- monitoring
  - on va utiliser [Netdata](https://www.netdata.cloud/)
- backup
  - un script maison qui fait appel à [Borg](https://borgbackup.readthedocs.io/en/stable/)

## Sommaire

- [Partie 3 : Maintien en condition opérationnelle](#partie-3--maintien-en-condition-opérationnelle)
  - [Sommaire](#sommaire)
- [I. Monitoring](#i-monitoring)
  - [1. Intro](#1-intro)
  - [2. Setup Netdata](#2-setup-netdata)
  - [3. Bonus : alerting](#3-bonus--alerting)
  - [4. Bonus : proxying](#4-bonus--proxying)
- [II. Backup](#ii-backup)

# I. Monitoring

## 1. Intro

[Netdata](https://www.netdata.cloud/) a plusieurs fonctions :

- **récolteur de données**
  - il récolte en temps réel des données sur le systèmes (utilisation CPU, RAM, fichiers de logs, etc)
- **interface web**
  - visualisation sexy en temps réel des données
- **alerting**
  - envoie des alertes, plein de choses compatibles comme Discord

> Discord ou autre chose, les systèmes de chat sont particulièrement adaptés à l'alerting pour avoir quelque chose de structuré, plutôt que le typique alerting par mail

**Netdata est simple de setup, très performant, et modulaire :**

- En effet, on va s'en servir pour tout faire, mais il peut s'inscrire comme une brique dans des infras plus grandes
- Il est réputé pour être un récolteur de données très performant : beaucoups de métriques récoltées, en peu de temps, pour un faible coût en perfs (on parle de "faible overhead")
- Ces métriques peuvent éventuellement être exportées vers un outil qui les centralisent

## 2. Setup Netdata

> Configuration sur la machine REVERSE-PROXY car elle est en frontale. Dans l'idéal, il faudrait le faire sur toutes.

🌞 **Installez Netdata** en exécutant la commande suivante :
#
```bash
[toto@proxy ~]$ bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
 --- Downloading static netdata binary: https://storage.googleapis.com/netdata-nightlies/netdata-latest.gz.run ---
[/tmp/netdata-kickstart-QH3gPvkXP4]$ curl -q -sSL --connect-timeout 10 --retry 3 --output /tmp/netdata-kickstart-QH3gPvkXP4/sha256sum.txt https://storage.googleapis.com/netdata-nightlies/sha256sums.txt
 OK

[/tmp/netdata-kickstart-QH3gPvkXP4]$ curl -q -sSL --connect-timeout 10 --retry 3 --output /tmp/netdata-kickstart-QH3gPvkXP4/netdata-latest.gz.run https://storage.googleapis.com/netdata-nightlies/netdata-latest.gz.run
 OK

 --- Installing netdata ---
[/tmp/netdata-kickstart-QH3gPvkXP4]$ sudo sh /tmp/netdata-kickstart-QH3gPvkXP4/netdata-latest.gz.run -- --auto-update

  ^
  |.-.   .-.   .-.   .-.   .  netdata
  |   '-'   '-'   '-'   '-'   real-time performance monitoring, done right!
  +----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+--->

  (C) Copyright 2017, Costa Tsaousis
  All rights reserved
  Released under GPL v3+
 
[...]
 OK

[/tmp/netdata-kickstart-QH3gPvkXP4]$ sudo rm /tmp/netdata-kickstart-QH3gPvkXP4/netdata-latest.gz.run
 OK

[/tmp/netdata-kickstart-QH3gPvkXP4]$ sudo rm -rf /tmp/netdata-kickstart-QH3gPvkXP4
 OK
```
#
> La méthode d'installation est peu orthodoxe, n'hésitez pas à me demander si vous voulez en savoir plus.

🌞 **Démarrez Netdata**

```bash
[toto@proxy ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
success
[toto@proxy ~]$ sudo firewall-cmd --reload
success
[toto@proxy ~]$ sudo systemctl start netdata
[toto@proxy ~]$ sudo systemctl enable netdata
[toto@proxy ~]$ sudo systemctl status netdata
● netdata.service - Real time performance monitoring
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-12-08 13:54:15 CET; 6min ago
 Main PID: 1908 (netdata)
    Tasks: 64 (limit: 4956)
   Memory: 148.2M
   CGroup: /system.slice/netdata.service
           ├─1908 /opt/netdata/bin/srv/netdata -P /opt/netdata/var/run/netdata/netdata.pid -D
           ├─1923 /opt/netdata/bin/srv/netdata --special-spawn-server
           ├─2074 /opt/netdata/usr/libexec/netdata/plugins.d/apps.plugin 1
           ├─2092 /opt/netdata/usr/libexec/netdata/plugins.d/go.d.plugin 1
           └─2099 /opt/netdata/usr/libexec/netdata/plugins.d/ebpf.plugin 1

Dec 08 13:54:15 proxy.tp2.cesi netdata[1908]: 2021-12-08 13:54:15: netdata INFO  : MAIN : CONFIG: cannot load cloud >
Dec 08 13:54:15 proxy.tp2.cesi [1908]: Found 0 legacy dbengines, setting multidb diskspace to 256MB
Dec 08 13:54:15 proxy.tp2.cesi netdata[1908]: 2021-12-08 13:54:15: netdata INFO  : MAIN : Found 0 legacy dbengines, >
Dec 08 13:54:15 proxy.tp2.cesi [1908]: Created file '/opt/netdata/var/lib/netdata/dbengine_multihost_size' to store >
Dec 08 13:54:15 proxy.tp2.cesi netdata[1908]: 2021-12-08 13:54:15: netdata INFO  : MAIN : Created file '/opt/netdata>
Dec 08 13:54:16 proxy.tp2.cesi [2099]: Does not have a configuration file inside `/opt/netdata/etc/netdata/ebpf.d.co>
Dec 08 13:54:16 proxy.tp2.cesi [2099]: Name resolution is disabled, collector will not parser "hostnames" list.
Dec 08 13:54:16 proxy.tp2.cesi [2099]: The network value of CIDR 127.0.0.1/8 was updated for 127.0.0.0 .
Dec 08 13:54:16 proxy.tp2.cesi [2099]: PROCFILE: Cannot open file '/opt/netdata/etc/netdata/apps_groups.conf'
Dec 08 13:54:16 proxy.tp2.cesi [2099]: Cannot read process groups configuration file '/opt/netdata/etc/netdata/apps_>

```

Bah voilà hein ! Ouvrez votre navigateur, go `http://<IP_VM>:19999` and enjoy.  
Prenez le temps de regarder un peu les métriques remontées. Certaines choses seront différentes suivant la machine où vous l'installez !

## 3. Bonus : alerting

🌞 **Mettez en place un alerting Discord**
#
- [lien de la doc](https://learn.netdata.cloud/docs/agent/health/notifications/discord) pour l'alerting Discord
- le principe est de recevoir les alertes dans un salon texte dédié sur un serveur Discord

```bash
[toto@proxy ~]$ sudo nano /opt/netdata/usr/lib/netdata/conf.d/health_alarm_notify.conf
[toto@proxy ~]$ sudo cat /opt/netdata/usr/lib/netdata/conf.d/health_alarm_notify.conf | grep DISCORD
SEND_DISCORD="YES"
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/918131884049965068/mieR3Yd1k49v5uuwbqZxhhq0fvTuOeQy96n6nw7bUPqz3-Oc_mvGkyjJSKy8tZ9owPTi"
DEFAULT_RECIPIENT_DISCORD="alarms"
[toto@proxy ~]$ sudo systemctl restart netdata
[toto@proxy ~]$ cd /opt/netdata/usr/libexec/netdata/plugins.d/
[toto@proxy plugins.d]$ ./alarm-notify.sh test
```



# II. Backup (Bonus)

[Borg](https://borgbackup.readthedocs.io/en/stable/) est un outil qui permet de réaliser des sauvegardes.

Il est performant et sécurisé :

- performant à l'utilisation
  - création de backup rapide, idem pour restauration
- les données sauvegardées sont chiffrées
- les données sauvegardées sont dédupliquées

Il repose sur un concept simple :

- on créer un "dépôt Borg" sur la machine avec une commande `borg init`
- c'est dans ce dépôt que pourront être stockées des sauvegardes
- on peut ensuite déclencher une sauvegarde d'un dossier donné vers le dépôt avec une commande `borg create`

Nous, on va écrire un script :

- il créer une nouvelle sauvegarde borg (avec `borg create` donc) d'un dossier précis, avec un nommage précis, dans un dépôt précis
- on en fera ensuite un service : un p'tit `backup.service`
- puis on fera en sorte que ce service se lance à intervalles réguliers

🌞 **Téléchargez et jouez avec Borg** sur la machine `web.tp2.cesi`

- créez un dépôt, faites des sauvegardes, restaures des sauvegardes, testez l'outil un peu

🌞 **Ecrire un script**

- il doit sauvegarder le dossier où se trouve NextCloud
- le dépôt borg doit être dans le dossier `/srv/backup/`
- le nom de la sauvegarde doit être `nextcloud_YYMMDD_HHMMSS`
- testez le à la main, avant de continuer

🌞 **Créer un service**

- créer un service `backup_db.service` qui exécute votre script
- ainsi, quand on lance le service, une backup de la base de données est déclenchée

La syntaxe, toujours la même :

```bash
[Unit]
Description=<DESCRIPTION>

[Service]
ExecStart=<COMMAND>
Type=oneshot

[Install]
WantedBy=multi-user.target
```

**NB : vous DEVEZ ajoutez la ligne `Type=oneshot` en dessous de la ligne `ExecStart=` dans votre service**. Cela indique au système que ce service ne sera pas un démon qui s'exécute en permanence, mais un script s'eécutera, puis qui aura une fin d'exécution (sinon le système peut par exemple essayer de relancer automatiquement un service qui s'arrête).

🌞 **Créer un timer**

- un *timer* c'est un fichier qui permet d'exécuter un service à intervalles réguliers
- créez un *timer* qui exécute le service `backup` toutes les heures

Pour cela, créer le fichier `/etc/systemd/system/backup.timer`.

> Notez qu'il est dans le même dossier que le service, et qu'il porte le même nom, mais pas la même extension.

Contenu du fichier `/etc/systemd/system/backup.timer` :

```bash
[Unit]
Description=Lance backup.service à intervalles réguliers
Requires=backup.service

[Timer]
Unit=backup.service
OnCalendar=hourly

[Install]
WantedBy=timers.target
```

> Au niveau du `OnCalendar`, on précise tout un tas de trucs : une heure précise une fois par semaine, toutes les trois heures les jours pairs (ui c'est improbable, mais on peut !), etc.

Activez maintenant le *timer* avec :

```bash
# on indique qu'on a modifié la conf du système
$ sudo systemctl daemon-reload

# démarrage immédiat du timer
$ sudo systemctl start backup.timer

# activation automatique du timer au boot de la machine
$ sudo systemctl enable backup.timer
```

🌞 **Vérifier que le *timer* a été pris en compte**, en affichant l'heure de sa prochaine exécution :

```bash
$ sudo systemctl list-timers
```



